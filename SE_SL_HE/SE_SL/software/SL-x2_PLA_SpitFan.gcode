; This gcode is formulated to work with dual tool head test stands running TAZ 6 Dual v2 firmware version 1.1.9.9
; Use of this gcode outside of an identical setup is likely to provide unexpected results
T0                           ; select extruder 1
M104 S205                    ; set temp to 205
T1                           ; select extruder 2
M104 S205                    ; set temp to 205
M117 FANS 100 PERCENT        ; display message
M106 P0 S255		     ; set cooling fan 0 to 100%
M106 P1 S255		     ; set cooling fan 1 to 100%
G4 S10			     ; dwell 10 seconds
M117 FANS OFF                ; display message
M106 P0 S0                   ; turn cooling fan 1 off
M106 P1 S0                   ; turn cooling fan 2 off
G4 S5			     ; dwell 5 seconds
M117 FANS 40 PERCENT         ; display message
M106 P0 S102		     ; set cooling fan 0 to 40%
M106 P1 S102		     ; set cooling fan 1 to 40%
G4 S10			     ; dwell 10 seconds
M117 FANS OFF                ; display message
M106 P0 S0 		     ; turn cooling fan 1 off
M106 P1 S0                   ; turn cooling fan 2 off
G4 S5			     ; dwell 5 seconds

M999                 ; clear errors
M400                 ; clear buffer
G21                  ; set units to millimeters
M82                  ; use absolute distances for extrusion
T0                   ; tool 0
G92 E0               ; Set cords to zero
T1                   ; tool 1
G92 E0               ; Set cords to zero
M92 T0 E420          ; set T0 esteps to base 420
M92 T1 E420          ; set T1 esteps to base 420
M500                 ; save into memory

T1                   ; select extruder 2
M104 S205            ; set temp to 205

T0                   ; select tool 0
M109 S205            ; set extruder nozzle to 205C and wait
M117 Extruding       ; display message
G1 E100 F80          ; move extruder 1 100mm
G4 S5                ; wait
M117 Retracting      ; display message
G92 E0               ; set E to 0
G1 E-45 F250         ; retract filament 45mm
G4 S1                ; wait
M300 S5              ; beep
M117 PULL FILAMENT   ; display message
G4 S5                ; wait
G92 E0               ; set E to 0
G1 E-45 F500         ; retract 45mm
G4 S1                ; wait

T1                   ; select tool 1
M109 S205            ; set extruder nozzle to 205C and wait
M117 Extruding       ; display message
G1 E100 F80          ; move extruder 1 100mm
G4 S5                ; wait
M117 Retracting      ; display message
G92 E0               ; set E to 0
G1 E-45 F250         ; retract filament 45mm
G4 S1                ; wait
M300 S5              ; beep
M117 PULL FILAMENT   ; display message
G4 S5                ; wait
G92 E0               ; set E to 0
G1 E-45 F500         ; retract 45mm
G4 S1                ; wait

M106 S255            ; turn E0 Fan on 100 percent
M106 P1 S255         ; turn E1 Fan on 100 percent

T0                   ; select extruder 1
M104 S60             ; set temp to 60
T1                   ; select extruder 2
M109 R60             ; wait for temp 
M106 P0 S0           ; fan off
M106 P1 S0           ; fan off
M84                  ; idle motors
M18                  ; turn off motors
M300 S5              ; beep for end
M117 Test Complete   ; display message

