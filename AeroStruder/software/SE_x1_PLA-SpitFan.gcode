; This gcode is formulated to work with dual tool head test stands running TAZ 6 Dual v2 firmware version 1.1.9.9
; Use of this gcode outside of an identical setup is likely to provide unexpected results
T0
M104 S205
M107            ; turn fan off
M117 FAN 100 PERCENT
M106 P1 S255    ; turn fan on full speed
G4 S15          ; wait 15 seconds
M117 FAN OFF
M106 P1 S0      ; turn fan off
M400            ; flush buffer
G4 S5           ; wait 5 seconds
M117 FAN 50 PERCENT
M106 P1 S127    ; turn fan on at 50
G4 S10          ; wait 10 seconds
M117 FAN OFF
M106 P1 S0      ; turn fan off

M999                 ; clear errors
M400                 ; clear buffer
G21                  ; set units to millimeters
M82                  ; use absolute distances for extrusion
G92 X0 Y0 Z0 E0      ; Set cords to zero
M92 E420             ; set esteps to base 420
M500                 ; save into memory

T0
M109 S205            ; set extruder nozzle to 205C and wait
M117 Extruding
G1 E100 F90          ; move extruder 1 100mm
G4 S5
M117 Retracting
G92 E0
G1 E-45 F250         ; retract filament 45mm
G4 S1
M300 S5
M117 PULL FILAMENT
G4 S5
G92 E0
G1 E-45 F500
G4 S1                ; wait

M106 P1 S255
M109 R60
M106 P1 S0           ; turn off fan
G4 S1                ; wait
M18                  ; turn off motors
M300 S5              ; beep for end

